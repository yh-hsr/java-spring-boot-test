package com.example.learn2.springbootlearn2.commom;

public class Result<T> {
    private int code;
    private String msg;
    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Result(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    //------ Success ------//
    public static Result<Boolean> success() {
        return new Result<Boolean>(200, "操作成功", true);
    }

    public static <T> Result<T> success(T data) {
        return new Result<>(200, "操作成功", data);
    }

    public static Result<Boolean> success(String msg) {
        return new Result<Boolean>(200, msg, true);
    }

    public static <T> Result<T> success(String msg, T data) {
        return new Result<T>(200, msg, data);
    }

    //------ Error ------//

    public static Result<Boolean> error() {
        return new Result<Boolean>(500, "操作失败", false);
    }

    public static <T> Result<Boolean> error(String msg) {
        return new Result<Boolean>(500, msg, false);
    }


}
