package com.example.learn2.springbootlearn2.commom;

import com.example.learn2.springbootlearn2.config.MinioProperties;
import com.example.learn2.springbootlearn2.exception.ServiceException;
import io.minio.*;
import io.minio.errors.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

@Component
public class MinioTemplate {
    @Autowired
    private MinioClient minioClient;
    @Autowired
    private MinioProperties minioProperties;

    /**
     * 上传文件
     * @param inputStream
     * @param contentType
     * @return
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public String putObject(InputStream inputStream, String contentType) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        try {
            // Make 'pactera' bucket if not exist.
            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket(minioProperties.getBucketName()).build());
            if (!found) {
                // Make a new bucket called 'pactera'.
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(minioProperties.getBucketName()).build());
            } else {
                System.out.println("Bucket 'pactera' already exists.");
            }
            // uploading
            ObjectWriteResponse pacteraResponse = minioClient.putObject(
                    PutObjectArgs.builder()
                            .bucket(minioProperties.getBucketName())
                            .object(UUID.randomUUID().toString())
                            .stream(inputStream, -1, 10485760)
                            .contentType(contentType).build()
            );
            return pacteraResponse.object();
        } catch (MinioException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * 删除文件
     * @return
     * @throws MinioException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public Boolean removeObject(String myObjectName) throws MinioException, IOException, NoSuchAlgorithmException, InvalidKeyException {
        // Remove object.
        minioClient.removeObject(
                RemoveObjectArgs.builder().bucket(minioProperties.getBucketName()).object(myObjectName).build());
        return true;
    }

    /**
     * 获取图片的get链接 bucket-name
     * @param myObjectName
     * @return
     * @throws MinioException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public String showAvatarImage(String myObjectName) throws MinioException, IOException, NoSuchAlgorithmException, InvalidKeyException {
        try {
//            "testimage"
            String objectUrl = minioClient.getObjectUrl(minioProperties.getBucketName(), myObjectName);
//        presignedGetObject getObjectUrl
//            minioClient.presignedGetObject("testimage", myObjectName);
            return objectUrl;
        } catch (MinioException e) {
            throw new ServiceException(e.getMessage());
        }
    }

}
