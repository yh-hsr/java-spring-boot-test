package com.example.learn2.springbootlearn2;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
@MapperScan(basePackages = "com.example.learn2.springbootlearn2.mapper")
public class SpringBootLearn2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootLearn2Application.class, args);
	}

}
