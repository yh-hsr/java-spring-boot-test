package com.example.learn2.springbootlearn2.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.learn2.springbootlearn2.commom.MinioTemplate;
import com.example.learn2.springbootlearn2.exception.ServiceException;
import com.example.learn2.springbootlearn2.mapper.UserMapper;
import com.example.learn2.springbootlearn2.model.dto.UserDataDto;
import com.example.learn2.springbootlearn2.model.entity.User;
import com.example.learn2.springbootlearn2.model.vo.UserDataVo;
import com.example.learn2.springbootlearn2.service.IUserService;
import io.minio.errors.MinioException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class IUserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private MinioTemplate minioTemplate;
    /**
     * 获取用户列表
     * @param name
     * @param age
     * @param email
     * @return List<UserDataVo>
     */
    @Override
    public List<UserDataVo> getUserList(String name, String age, String email) {
        System.out.println("IUserServiceImpl->getUserList("+name+","+age+","+email+")");
        userMapper.selectList(new QueryWrapper<User>().eq("name",name));
        return userMapper.getUserList(name, age, email);
    }

    /**
     * 根据ID获取用户详细信息
     * @param id
     * @return UserDataVo
     */
    @Cacheable(cacheNames = "getUserDetail", key = "#id")
    @Override
    public UserDataVo getUserDetail(Integer id) {
        System.out.println("数据库查询");
        System.out.println("IUserServiceImpl->getUserDetail");
        if (id == null) {
            return null;
        }
        UserDataVo userData = userMapper.getUserDetail(id);
        try {
            if (userData.getAvatar() != "") {
                userData.setAvatar(minioTemplate.showAvatarImage(userData.getAvatar()));
            }
        } catch ( MinioException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new ServiceException(e.getMessage());
        }

        return userData;
    }

    /**
     * 插入单条用户信息
     * @param dto
     * @return id
     */
    @Transactional
    @Override
    public Integer insertOneUser(UserDataDto dto) {
        System.out.println("IUserServiceImpl->insertOneUser");
        if (dto == null) {
            throw new ServiceException("数据为空");
        }
        if (Objects.equals(dto.getName(), "") || dto.getName() == null) {
            throw new ServiceException("Name未输入");
        }
        if ("admin".equals(dto.getName())) {
            throw new ServiceException("admin用户不允许添加");
        }
        if (Objects.equals(dto.getEmail(), "") || dto.getEmail() == null) {
            System.out.println("dto.getEmail() == null");
            throw new ServiceException("Email未输入");
        }
        if (dto.getAge() == null) {
            dto.setAge(0);
        }
        User insUserData = new User();
        insUserData.setName(dto.getName());
        insUserData.setAge(dto.getAge());
        insUserData.setEmail(dto.getEmail());
        insUserData.setAvatar(dto.getAvatar());
        Integer flag = 0;
        Integer id = 0;
        flag = userMapper.insertOneUser(insUserData);
        if (flag > 0) {
            id = insUserData.getId();
            System.out.println("id=" + id);
        }
        return id;
    }

    /**
     * 插入多条用户信息
     * @param dtoList
     * @return
     */
    public Integer insertMoreUser(List<UserDataDto> dtoList) {
        System.out.println("IUserServiceImpl->insertMoreUser");
        List<User> userList = new ArrayList<>();
        for (UserDataDto dto : dtoList) {
            if (Objects.equals(dto.getName(), "") || dto.getName() == null) {
                System.out.println("dto.getName() == null");
                throw new ServiceException("Name未输入");
            }
            if (Objects.equals(dto.getEmail(), "") || dto.getEmail() == null) {
                System.out.println("dto.getEmail() == null");
                throw new ServiceException("Email未输入");
            }
            if (dto.getAge() == null) {
                dto.setAge(0);
            }

            User user = new User();
            user.setName(dto.getName());
            user.setAge(dto.getAge());
            user.setEmail(dto.getEmail());
            user.setAvatar(dto.getAvatar());
            userList.add(user);
        }
        Integer id = 0;
        id = userMapper.insertMoreUser(userList);
        System.out.println("id=" + id);
        return id;
    }

    /**
     * 更新单条用户信息
     * @param dto
     * @return
     */
    @CacheEvict(cacheNames = "getUserDetail", key = "#dto.id")
    public Integer updateUser(UserDataDto dto) {
        System.out.println("IUserServiceImpl->insertMoreUser");
        if (dto == null) {
            throw new ServiceException("数据为空");
        }
        if (dto.getId() == null) {
            throw new ServiceException("Id未输入");
        }
        if (Objects.equals(dto.getName(), "") || dto.getName() == null) {
            throw new ServiceException("Name未输入");
        }
        if (Objects.equals(dto.getEmail(), "") || dto.getEmail() == null) {
            throw new ServiceException("Email未输入");
        }
        User user = new User();
        user.setId(dto.getId());
        user.setName(dto.getName());
        user.setEmail(dto.getEmail());
        user.setAvatar(dto.getAvatar());
        Integer id = 0;
        id = userMapper.updateUser(user);
        System.out.println("id=" + id);
        return id;
    }

    /**
     * 删除单个用户
     * @param id
     * @return
     */
    @CacheEvict(cacheNames = "getUserDetail", key = "#id")
    public Integer deleteOneUser(Integer id) {
        if (id == null || id == 0 ) {
            throw new ServiceException("用户id不能为空或者是0");
        }
        UserDataVo userDetailData = userMapper.getUserDetail(id);

        if (userDetailData == null) {
            throw new ServiceException("用户id不存在");
        }
        if (userDetailData.getAvatar() != "") {
            Boolean removeFlag = true;
            try {
                removeFlag = minioTemplate.removeObject(userDetailData.getAvatar());
            } catch (IOException | NoSuchAlgorithmException | InvalidKeyException | MinioException e) {
                throw new ServiceException(e.getMessage());
            }

        }
        Integer delFlag = 0;
        delFlag = userMapper.deleteOneUser(id);
        if (delFlag <= 0) {
            throw new ServiceException("用户id（" + id + "）删除失败");
        }
        return delFlag;
    }

    /**
     * 更新头像
     * @param id
     * @param file
     * @return
     */
    @CacheEvict(value = "getUserDetail", key = "#id")
    public Boolean uploadAvatar(Integer id, MultipartFile file) {
        String filePath = "";
        UserDataVo userData = userMapper.getUserDetail(id);
        if(userData == null) {
            throw new ServiceException("（id=" + id + "）用户不存在");
        }

        try {
            //如果图片存在，删除图片
            if (userData.getAvatar() != "") {
                System.out.println(userData.getAvatar());
                minioTemplate.removeObject(userData.getAvatar());
            }
        } catch (MinioException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new ServiceException(e.getMessage());
        }
        try {
            filePath = minioTemplate.putObject(file.getInputStream(), file.getContentType());
        }
        catch (IOException | NoSuchAlgorithmException | InvalidKeyException e){
            throw new ServiceException(e.getMessage());
        }
        Integer updateFlag = userMapper.uploadAvatar(id, filePath);
        if (updateFlag <= 0) {
            throw new ServiceException("用户id（" + id + "）头像更新失败");
        }
        return true;
    }
}
