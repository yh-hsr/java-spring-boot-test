package com.example.learn2.springbootlearn2.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.learn2.springbootlearn2.model.dto.UserDataDto;
import com.example.learn2.springbootlearn2.model.entity.User;
import com.example.learn2.springbootlearn2.model.vo.UserDataVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IUserService extends IService<User> {

    /**
     * 获取用户列表
     * @param name
     * @param age
     * @param email
     * @return List<UserDataVo>
     */
    public List<UserDataVo> getUserList(String name, String age, String email);

    /**
     * 根据ID获取用户详细信息
     * @param id
     * @return UserDataVo
     */
    public UserDataVo getUserDetail(Integer id);

    /**
     * 插入单条用户信息
     * @param dto
     * @return id
     */
    public Integer insertOneUser(UserDataDto dto);

    /**
     * 插入多条用户信息
     * @param dtoList
     * @return
     */
    public Integer insertMoreUser(List<UserDataDto> dtoList);

    /**
     * 更新单条用户信息
     * @param dto
     * @return
     */
    public Integer updateUser(UserDataDto dto);

    /**
     * 删除单个用户
     * @param id
     * @return
     */
    public Integer deleteOneUser(Integer id);

    public Boolean uploadAvatar(Integer id, MultipartFile file);
}
