package com.example.learn2.springbootlearn2.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.learn2.springbootlearn2.model.dto.UserDataDto;
import com.example.learn2.springbootlearn2.model.entity.User;
import com.example.learn2.springbootlearn2.model.vo.UserDataVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import javax.websocket.server.PathParam;
import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {

    /**
     * 获取用户列表
     * @param name
     * @param age
     * @param email
     * @return
     */
    List<UserDataVo> getUserList(String name, String age, String email);

    /**
     * 根据ID获取用户详细信息
     * @param id
     * @return
     */
    UserDataVo getUserDetail(Integer id);

    /**
     * 插入单条用户信息
     * @param user
     * @return 成功条数
     */
    Integer insertOneUser(@Param("user") User user);

    /**
     * 插入多条用户信息
     * @param userList
     * @return 成功条数
     */
    Integer insertMoreUser(@Param("userList") List<User> userList);

    /**
     * 更新单条用户信息
     * @param user
     * @return
     */
    Integer updateUser(@Param("user") User user);

    /**
     * 单用户删除
     * @param id
     * @return
     */
    Integer deleteOneUser(Integer id);

    /**
     * 更新头像
     * @param id
     * @param avatar
     * @return
     */
    Integer uploadAvatar(Integer id, String avatar);

    IPage<UserDataVo> selectUserPageVo(IPage<UserDataVo> page, Integer state);
}
