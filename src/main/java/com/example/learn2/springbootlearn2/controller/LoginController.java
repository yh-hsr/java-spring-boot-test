package com.example.learn2.springbootlearn2.controller;

import com.example.learn2.springbootlearn2.model.dto.UserDataDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("")
public class LoginController {
    @GetMapping("login")
    public String goToLoginPage(Model model) {
        System.out.println("1111111");
        List<String> usernames = new ArrayList<>();
        usernames.add("LiSa");
        usernames.add("Joey");
        usernames.add("Sam");
        model.addAttribute("usernames", usernames);
        model.addAttribute("pageTitle", "LoginPage");
        return "goTologin.html";
    }

    @PostMapping("login")
    public String loginPage(@ModelAttribute UserDataDto user) {
//        System.out.println(user.getUsername());
//        System.out.println(user.getPassword());
        return "login.html";
    }

    @RequestMapping("hello")
    public String hello(Model m) throws Exception {
        m.addAttribute("now", DateFormat.getDateTimeInstance().format(new Date()));
        if(true){
            throw new Exception("some exception");
        }
        return "hello";
    }
}
