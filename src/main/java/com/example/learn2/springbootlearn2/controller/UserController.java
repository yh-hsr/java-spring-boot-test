package com.example.learn2.springbootlearn2.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.learn2.springbootlearn2.commom.Result;
import com.example.learn2.springbootlearn2.model.dto.UserDataDto;
import com.example.learn2.springbootlearn2.model.entity.User;
import com.example.learn2.springbootlearn2.model.vo.UserDataVo;
import com.example.learn2.springbootlearn2.service.impl.IUserServiceImpl;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {
    public static final int PAGE_SIZE = 5;
    @Resource
    private IUserServiceImpl iUserService;

    /**
     * 按条件获取用户列表
     * url格式 user?name=t&age=8&email=11
     * @param name
     * @param age
     * @param email
     * @return
     */
    @GetMapping("/user")
    public Result getUserList(Integer current, String name, String age, String email) {
        Boolean whereNameFlag = true;
        Boolean whereAgeFlag = true;
        Boolean whereEmailFlag = true;
        if (current == null) {
            current = 0;
        }
        if (name == "" || name == null) {
            whereNameFlag = false;
        }
        if (age == "" || age == null) {
            whereAgeFlag = false;
        }
        if (email == "" || email == null) {
            whereEmailFlag = false;
        }
//        long pageCount = 0;
//        pageCount = iUserService.count(new QueryWrapper<User>().like(whereNameFlag,"name", name).like(whereAgeFlag,"age", age).like(whereEmailFlag,"email", email));
//        System.out.println("------" + pageCount + "------");
        Page<User> page = new Page<User>(current,PAGE_SIZE);
        IPage<User> userData = null;
        return Result.success("用户列表获取成功", iUserService.page(page, new QueryWrapper<User>().like(whereNameFlag,"name", name).like(whereAgeFlag,"age", age).like(whereEmailFlag,"email", email)));
    }

    /**
     * 按ID获取用户详细信息
     * url格式 userDetail/2
     * @param id
     * @return
     */
    @GetMapping("/userDetail/{id}")
    public Result getUserDetail(@PathVariable Integer id) {
        return Result.success("用户详情获取成功", iUserService.getById(id));
    }

    /**
     * 单用户数据插入
     * @param dto
     * @return
     */
    @PostMapping("/postUser")
    public Result insertUsers(@RequestBody UserDataDto dto) {
        Integer insUserFlag = 0;
        insUserFlag = iUserService.insertOneUser(dto);
        if (insUserFlag > 0) {
            return Result.success("单行数据插入成功");
        } else {
            return Result.error("单行数据插入失败");
        }
    }

    /**
     * 多用户数据插入
     * @param dto
     * @return
     */
    @PostMapping("/postUserList")
    public Result insertUserList(@RequestBody List<UserDataDto> dto) {
        Integer insMoreUserFlag = 0;
        insMoreUserFlag = iUserService.insertMoreUser(dto);
        if (insMoreUserFlag > 0) {
            return Result.success("多行数据插入成功");
        } else {
            return Result.error("多行数据插入失败");
        }
    }

    /**
     * 更新单条用户信息
     * @param dto
     * @return
     */
    @PutMapping("/putUser")
    public Result upUserData(@RequestBody UserDataDto dto) {
        if (iUserService.updateUser(dto) > 0) {
            return Result.success("用户数据更新成功");
        }
        return Result.error();
    }

    /**
     * 删除单条用户信息
     * @param id
     * @return
     */
    @DeleteMapping("/deleteUser/{id}")
    public Result deleteUser(@PathVariable Integer id) {
        System.out.println("delId=>" + id);
        if (iUserService.deleteOneUser(id) > 0) {
            return Result.success();
        }
        return Result.error();
    }


    /**
     * 更新头像
     * @param id
     * @param file
     * @return
     */
    @PostMapping("/avatar")
    public Result uploadAvatar(@RequestParam("id") Integer id, @RequestParam("file") MultipartFile file) {
        if (iUserService.uploadAvatar(id, file) != true) {
            return Result.error("头像更新失败");
        }
        return Result.success();
    }
}
